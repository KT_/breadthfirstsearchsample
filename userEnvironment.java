package breadthFirstSearch;
import java.util.*;
public class userEnvironment {
	private ArrayList<User> allUsers = new ArrayList<User>();
	private ArrayList<User> alternativeAll = new ArrayList<User>();
	public userEnvironment() {
		
	}
	public void createRandomizedUsers(int number_of_users) {
		Random rand = new Random();
		for (int i=0;i<number_of_users;i++) {
			String password = "";
			for (int j=0;j<6;j++) {
				int character = rand.nextInt(10);
				password += String.valueOf(character);
			}
			User u = new User("u"+i,password);
			this.allUsers.add(u);
		}
	}
	
	public void addRandomConnection(int interval) {
		for (int i=0;i<this.allUsers.size();i++) {
			Random rand = new Random();
			int connectionCount = rand.nextInt(interval)+1;
			for (int j=0;j<connectionCount;j++) {
				int userIndex = rand.nextInt(this.allUsers.size());
				if(userIndex!=i && this.allUsers.get(i).getConnections().contains(this.allUsers.get(userIndex))==false) {
					this.allUsers.get(i).addConnection(this.allUsers.get(userIndex));
					this.allUsers.get(userIndex).addConnection(this.allUsers.get(i));
				}
			}
		}
	}

	public ArrayList<User> shortestConnectionPath(User user,User targetuser) {
		int index = 0;
		HashMap<User,ArrayList<User>> connectionMap = new HashMap<User,ArrayList<User>>();
		
		ArrayList<User> users = new ArrayList<User>();
		ArrayList<User> route = new ArrayList<User>();
		users.add(user);
		if(user.equals(targetuser)) {
			route.add(user);
			return route;
		}
		else {
			for (int i=0;i<users.size();i++) {		
			    
				
				if(users.get(i).getConnections().contains(targetuser)) {				
					ArrayList<User> next;
					User newtarget = users.get(i);
					next=this.shortestConnectionPath(user,newtarget);
					for (int x=0;x<=next.size()-1;x++) {
						if(route.contains(next.get(x))==false) {
							route.add(next.get(x));
						}
						System.out.println();
					}
					route.add(targetuser);
					return route;
				}
			
				for (int j=0;j<users.get(i).getConnections().size();j++) {
					if(users.contains(users.get(i).getConnections().get(j))==false) {
						users.add(users.get(i).getConnections().get(j));	
					}				
				}	
			}
			return route;
			
		}
	}
	public ArrayList<User> getList() {
		return this.allUsers;
	}
	
}
