package breadthFirstSearch;
import java.util.*;
public class User {
	private ArrayList<User> connections = new ArrayList<User>();
	public String name;
	private String password;
	public User(String name,String password) {
		this.name=name;
		this.password=password;
	}
	public void addConnection(User user) {
		this.connections.add(user);
	}
	public ArrayList<User> getConnections() {
		return this.connections;
	}
	

}
